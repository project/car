<?php

/**
 * Modules should return this value from hook_car_access() to allow access to a car.
 */
define('CAR_ACCESS_ALLOW', 'allow');

/**
 * Modules should return this value from hook_car_access() to deny access to a car.
 */
define('CAR_ACCESS_DENY', 'deny');

/**
 * Modules should return this value from hook_car_access() to not affect car access.
 */
define('CAR_ACCESS_IGNORE', NULL);

/**
 * Implements hook_hook_info().
 */
function car_hook_info() {
  $hooks = array(
    'car_load',
    'car_presave',
    'car_insert',
    'car_update',
    'car_delete',
  );

  $hooks = array_fill_keys($hooks, array('group' => 'car'));
  return $hooks;
}

/**
 * Implements hook_entity_info().
 */
function car_entity_info() {
  $info['car'] = array(
    'label' => t('Car'),
    'controller class' => 'DrupalDefaultEntityController',
    'base table' => 'car',
    'uri callback' => 'car_uri',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'cid',
      'label' => 'name',
    ),
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label' => t('Full content'),
        'custom settings' => FALSE,
      ),
    ),
    'token type' => 'car',
  );

  return $info;
}

/**
 * Implements hook_permission().
 */
function car_permission() {

}

/**
 * Implements hook_menu().
 */
function car_menu() {
  $items['car/%car'] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('car', 1),
    'page callback' => 'car_view',
    'page arguments' => array(1),
    'access callback' => 'car_access',
    'access arguments' => array('view', 1),
    'file' => 'car.admin.inc',
  );
  $items['car/%car/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['car/%car/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('car_edit_form', 1),
    'access callback' => 'car_access',
    'access arguments' => array('update', 1),
    'file' => 'car.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'weight' => 10,
  );
  $items['car/%car/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('car_delete_form', 1),
    'access callback' => 'car_access',
    'access arguments' => array('delete', 1),
    'file' => 'car.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'weight' => 20,
  );

  $items['car/add'] = array(
    'title' => 'Add car',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('car_edit_form', NULL),
    'access callback' => 'car_access',
    'access arguments' => array('create', 'car'),
    'file' => 'car.admin.inc',
    'type' => MENU_SUGGESTED_ITEM,
  );

  $items['user/%user/cars'] = array(
    'title' => 'Cars',
    'page callback' => 'car_list_page',
    'access callback' => TRUE,
    'file' => 'car.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );
  $items['user/%user/cars/add'] = $items['car/add'];
  $items['user/%user/cars/add']['type'] = MENU_LOCAL_ACTION;
  $items['user/%user/cars/add']['page arguments'] = array('car_edit_form', NULL, 1);

  return $items;
}

/**
 * Implements hook_theme().
 */
function car_theme() {
  return array(
    'car' => array(
      'render element' => 'elements',
      'template' => 'car',
    ),
  );
}

function template_preprocess_car(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['car'] = $variables['elements']['#car'];
  $car = $variables['car'];

  //$variables['date']      = format_date($node->created);
  $variables['name']      = theme('username', array('account' => user_load($car->uid)));

  $uri = entity_uri('car', $car);
  $variables['car_url']  = url($uri['path'], $uri['options']);
  $label = entity_label('car', $car);
  $variables['title']     = check_plain($label);
  $variables['page']      = $variables['view_mode'] == 'full' && car_is_page($car);

  // Flatten the car object's member fields.
  $variables = array_merge((array) $car, $variables);

  // Helpful $content variable for templates.
  foreach (element_children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Make the field variables available with the appropriate language.
  field_attach_preprocess('car', $car, $variables['content'], $variables);

  // Display post information only on certain node types.
  //if (variable_get('node_submitted_' . $node->type, TRUE)) {
  //  $variables['display_submitted'] = TRUE;
  //  $variables['submitted'] = t('Submitted by !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
  //  $variables['user_picture'] = theme_get_setting('toggle_node_user_picture') ? theme('user_picture', array('account' => $node)) : '';
  //}
  //else {
  //  $variables['display_submitted'] = FALSE;
  //  $variables['submitted'] = '';
  //  $variables['user_picture'] = '';
  //}

  // Clean up name so there are no underscores.
  $variables['theme_hook_suggestions'][] = 'car__' . $car->cid;

  // Gather car attributes.
  $variables['attributes_array']['class'][] = 'car';
  $variables['attributes_array']['id'] = 'car-' . $car->cid;
}

/**
 * Entity uri callback for car entities.
 *
 * @param $car
 *   A fully-loaded car entity.
 */
function car_uri($car) {
  return array(
    'path' => "car/{$car->cid}",
  );
}

function car_access($op, $car, $account = NULL) {
   global $user;

  $rights = &drupal_static(__FUNCTION__, array());

  if (!$car || !in_array($op, array('view', 'update', 'delete', 'create'), TRUE)) {
    // If there was no car to check against, or the $op was not one of the
    // supported ones, we return access denied.
    return FALSE;
  }
  // If no user object is supplied, the access check is for the current user.
  if (empty($account)) {
    $account = $user;
  }

  // $car may be either an object or a car type. Since car types cannot be
  // an integer, use either cid or type as the static cache id.
  $cid = is_object($car) ? $car->cid : $car;

  // If we've already checked access for this car, user and op, return from
  // cache.
  if (isset($rights[$account->uid][$cid][$op])) {
    return $rights[$account->uid][$cid][$op];
  }

  if (user_access('bypass car access', $account)) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }
  if (!user_access('access cars', $account)) {
    $rights[$account->uid][$cid][$op] = FALSE;
    return FALSE;
  }

  // We grant access to the car if both of the following conditions are met:
  // - No modules say to deny access.
  // - At least one module says to grant access.
  // If no module specified either allow or deny, we fall back to car owner
  // access.
  $access = module_invoke_all('car_access', $car, $op, $account);
  if (in_array(CAR_ACCESS_DENY, $access, TRUE)) {
    $rights[$account->uid][$cid][$op] = FALSE;
    return FALSE;
  }
  elseif (in_array(CAR_ACCESS_ALLOW, $access, TRUE)) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }

  // Owners can always view, update, and delete their own cars.
  if ($op != 'create' && $account->uid == $car->uid && !empty($account->uid)) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }

  return FALSE;
}

/**
 * Loads a car object.
 *
 * @param $cid
 *   A car ID.
 */
function car_load($cid) {
  $cars = entity_load('car', array($cid));
  return !empty($cars) ? reset($cars) : FALSE;
}

/**
 * Loads multiple car objects.
 *
 * @param $cids
 *   An array of car IDs.
 */
function car_load_multiple(array $cids) {
  return entity_load('car', $cids);
}

/**
 * Saves changes to a car or add a new car.
 *
 * @param $car
 *   A car object to be saved. New cars should have an empty value for
 *   $car->cid or not have the car ID property defined. When updating an
 *   existing car, the $car->cid property should be an existing car ID.
 */
function car_save($car) {
  $transaction = db_transaction();

  try {
    $result = NULL;

    // Load the stored entity, if any.
    if (!empty($car->cid) && !isset($car->original)) {
      $car->original = entity_load_unchanged('car', $car->cid);
    }

    field_attach_presave('car', $car);
    global $user;

    // Determine if we will be inserting a new car.
    if (!isset($car->is_new)) {
      $car->is_new = empty($car->cid);
    }

    // Let modules modify the car before it is saved to the database.
    module_invoke_all('car_presave', $car);
    module_invoke_all('entity_presave', $car, 'car');

    // Save the car and its fields, and then invoke the insert or update hooks.
    if ($car->is_new) {
      drupal_write_record('car', $car);
      field_attach_insert('car', $car);
      module_invoke_all('car_insert', $car);
      module_invoke_all('entity_insert', $car, 'car');
      $result = SAVED_NEW;
    }
    else {
      drupal_write_record('car', $car, array('cid'));
      field_attach_update('car', $car);
      module_invoke_all('car_update', $car);
      module_invoke_all('entity_update', $car, 'car');
      $result = SAVED_UPDATED;
    }

    // Clear internal properties.
    unset($car->is_new);
    unset($car->original);

    // Clear the static loading cache.
    entity_get_controller('car')->resetCache(array($car->cid));

    // Clear page and block caches.
    cache_clear_all();

    // Ignore slave server temporarily to give time for the
    // saved carto be propagated to the slave.
    db_ignore_slave();

    return $result;
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('car', $e);
    throw $e;
  }
}

/**
 * Deletes a car.
 *
 * @param $cid
 *   A car ID.
 *
 * @see car_delete_multiple()
 */
function car_delete($cid) {
  return car_delete_multiple(array($cid));
}

/**
 * Deletes multiple cars.
 *
 * @param $cids
 *   An array of car IDs.
 *
 * @see hook_car_delete()
 * @see hook_entity_delete()
 */
function car_delete_multiple(array $cids) {
  $transaction = db_transaction();
  
  if (!empty($cids) && $cars = car_load_multiple($cids)) {
    try {
      foreach ($cars as $cid => $car) {
        module_invoke_all('car_delete', $car);
        module_invoke_all('entity_delete', $car, 'car');
        field_attach_delete('car', $car);
      }

      db_delete('car')
       ->condition('cid', $cids, 'IN')
       ->execute();

      //foreach ($cars as $car) {
      //  watchdog('car', 'Deleted car @cid: %name.', array('@cid' => $car->cid, '%name' => $car->name));
      //}
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('car', $e);
      throw $e;
    }

    // Clear the static cache.
    entity_get_controller('car')->resetCache();
  }
}

function car_build_content($car, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Remove previously built content, if exists.
  $car->content = array();

  // Build fields content.
  // In case of a multiple view, car_view_multiple() already ran the
  // 'prepare_view' step. An internal flag prevents the operation from running
  // twice.
  field_attach_prepare_view('car', array($car->cid => $car), $view_mode, $langcode);
  entity_prepare_view('car', array($car->cid => $car), $langcode);
  $car->content += field_attach_view('car', $car, $view_mode, $langcode);

  // Build links.
  $links = array();
  $car->content['links'] = array(
    '#theme' => 'links__car',
    '#pre_render' => array('drupal_pre_render_links'),
    '#attributes' => array('class' => array('links', 'inline')),
  );
  $car->content['links']['car'] = array(
    '#theme' => 'links__car__car',
    '#links' => $links,
    '#attributes' => array('class' => array('links', 'inline')),
  );

  // Allow modules to make their own additions to the car.
  module_invoke_all('car_view', $car, $view_mode, $langcode);
  module_invoke_all('entity_view', $car, 'car', $view_mode, $langcode);
}

function car_view($car, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Populate $car->content with a render() array.
  car_build_content($car, $view_mode, $langcode);

  $build = $car->content;
  // We don't need duplicate rendering info in node->content.
  unset($car->content);

  $build += array(
    '#theme' => 'car',
    '#car' => $car,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  );

  $build['content'] = array(
    '#markup' => 'This is a car! oooo',
  );

  // Add contextual links for this car, except when the car is already being
  // displayed on its own page. Modules may alter this behavior (for example,
  // to restrict contextual links to certain view modes) by implementing
  // hook_car_view_alter().
  if (!empty($car->cid) && !($view_mode == 'full' && car_is_page($car))) {
    $build['#contextual_links']['car'] = array('car', array($car->cid));
  }

  // Allow modules to modify the structured node.
  $type = 'car';
  drupal_alter(array('car_view', 'entity_view'), $build, $type);

  return $build;
}

function car_view_multiple($cars, $view_mode = 'teaser', $weight = 0, $langcode = NULL) {
  field_attach_prepare_view('car', $cars, $view_mode, $langcode);
  entity_prepare_view('car', $cars, $langcode);
  $build = array();
  foreach ($cars as $car) {
    $build['cars'][$car->cid] = car_view($car, $view_mode, $langcode);
    $build['cars'][$car->cid]['#weight'] = $weight;
    $weight++;
  }
  $build['cars']['#sorted'] = TRUE;
  return $build;
}

function car_is_page($car) {
  return ($object = menu_get_object('car', 1)) && $object->cid == $car->cid;
}
