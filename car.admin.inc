<?php

function car_list_page($account = NULL) {
  if (!isset($account)) {
    $account = user_load($GLOBALS['user']->uid);
  }

  $cids = db_query("SELECT cid FROM {car} WHERE uid = :uid", array(':uid' => $account->uid))->fetchCol();
  $cars = car_load_multiple($cids);

  if (!empty($cars)) {
    $build = car_view_multiple($cars);
  }
  else {
    $build['no_cars'] = array(
      '#markup' => t('The user has no cars.'),
    );
  }

  return $build;
}

/**
 * Edits a car.
 */
function car_edit_form($form, &$form_state, $car = NULL, $account = NULL) {
  // During initial form build, add the car entity to the form state for use
  // during form building and processing. During a rebuild, use what is in the
  // form state.
  if (!isset($form_state['car'])) {
    if (!isset($car)) {
      $car = new stdClass();
      $car->cid = NULL;
      $car->uid = isset($account->uid) ? $account->uid : $GLOBALS['user']->uid;
      $car->name = '';
    }
    $form_state['car'] = $car;
  }
  else {
    $car = $form_state['car'];
  }

  $form['#attributes']['class'][] = 'car-form';

  // Basic car information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('cid', 'uid') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($car->$key) ? $car->$key : NULL,
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $car->name,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : ($car->cid ? 'car/' . $car->cid : '<front>'),
    '#title' => t('Cancel'),
    '#weight' => 10,
  );

  field_attach_form('car', $car, $form, $form_state);
  return $form;
}

function car_edit_form_validate($form, &$form_state) {

}

function car_edit_form_submit($form, &$form_state) {
  $car = $form_state['car'];
  entity_form_submit_build_entity('car', $car, $form, $form_state);
  $result = car_save($car);

  if ($result === SAVED_NEW) {
    drupal_set_message(t('Added new car %name.', array('%name' => entity_label('car', $car))));
    //watchdog('car', 'Added new car @cid: %name.', array('@cid' => $car->cid, '%name' => $car->name), WATCHDOG_NOTICE, l(t('View'), 'car/' . $car->cid));
    $form_state['values']['cid'] = $car->cid;
    $form_state['cid'] = $car->cid;
  }
  elseif ($result === SAVED_UPDATED) {
    drupal_set_message(t('Saved car %name.', array('%name' => entity_label('car', $car))));
    //watchdog('car', 'Updated car @cid: %name.', array('@cid' => $car->cid, '%name' => $car->name), WATCHDOG_NOTICE, l(t('View'), 'car/' . $car->cid));
  }
  else {
    // In the unlikely case something went wrong on save, the car will be
    // rebuilt and car form redisplayed.
    drupal_set_message(t('The car could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
    return;
  }

  // Redirect to the car page by default.
  $form_state['redirect'] = 'car/' . $car->cid;
}

function car_delete_form($form, &$form_state, $car) {
  $form['#car'] = $car;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $car->cid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => entity_label('car', $car))),
    'car/' . $car->cid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function car_delete_form_submit($form, &$form_state) {
  dpm($form_state);
  if ($form_state['values']['confirm']) {
    $car = car_load($form_state['values']['cid']);
    car_delete($form_state['values']['cid']);
    drupal_set_message(t('Deleted car %name.', array('%name' => entity_label('car', $car))));
  }

  $form_state['redirect'] = '<front>';
}
